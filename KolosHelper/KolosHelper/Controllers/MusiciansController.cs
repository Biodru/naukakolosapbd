using System.Threading.Tasks;
using KolosHelper.DTOs;
using KolosHelper.Services;
using Microsoft.AspNetCore.Mvc;

namespace KolosHelper.Controllers
{
    [ApiController]
    [Microsoft.AspNetCore.Components.Route("api/musicians")]
    public class MusiciansController : ControllerBase
    {
        private readonly IDbService _db;

        public MusiciansController(IDbService db)
        {
            this._db = db;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> ShowMusicianById([FromRoute] int id)
        {
            var result = await _db.GetmusicianById(id);
            return result.Exists ? Ok(result.Musician) : NotFound(result.Message);
        }
        [Route("add")]
        [HttpPut]
        public async Task<IActionResult> AddNewMusician([FromBody] AddMusicianAndTrackRequestDto musician)
        {
            var result = await _db.AddMusician(musician);
            return result.Added ? Ok(result.Message) : BadRequest(result.Message);
        }

    }
}