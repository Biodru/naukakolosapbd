namespace KolosHelper.DTOs
{
    public class AddMusicianAndTrackRequestDto
    {
        public int IdMusician { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nickname { get; set; }
        public string TrackName { get; set; }
        public double Duration { get; set; }
        public int IdTrack { get; set; }
    }
}