namespace KolosHelper.DTOs
{
    public class AddMusicianAndTrackResponseDto
    {
        public bool Added { get; set; }
        public string Message { get; set; }
    }
}