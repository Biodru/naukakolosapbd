using KolosHelper.Models;

namespace KolosHelper.DTOs
{
    public class GetMusicianByIdResponseDto
    {
        public bool Exists { get; set; }
        public Musician Musician { get; set; }
        public string Message { get; set; }
    }
}