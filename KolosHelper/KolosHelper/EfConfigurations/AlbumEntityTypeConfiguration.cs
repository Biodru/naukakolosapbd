using KolosHelper.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KolosHelper.EfConfigurations
{
    public class AlbumEntityTypeConfiguration : IEntityTypeConfiguration<Album>
    {
        public void Configure(EntityTypeBuilder<Album> builder)
        {
            //MARK: Konfiguracja, czyli klucz, którego wartość się zwiększa, pola wymagane, długość itp
            builder.HasKey(e => e.IdAlbum);
            builder.Property(e => e.IdAlbum).ValueGeneratedOnAdd();
            builder.Property(e => e.AlbumName).IsRequired().HasMaxLength(30);
            builder.Property(e => e.PublishDate).IsRequired();
            
            //MARK: Powiązania, stworzenie relacji i określenie klucza obcegoą
            builder.HasOne(e => e.MusicLabel).WithMany(m => m.Albums).HasForeignKey(e => e.IdMusicLabel);
            builder.HasMany<Track>(e => e.Tracks);
        }
    }
}