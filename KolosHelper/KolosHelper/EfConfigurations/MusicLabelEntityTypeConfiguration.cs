using KolosHelper.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KolosHelper.EfConfigurations
{
    public class MusicLabelEntityTypeConfiguration : IEntityTypeConfiguration<MusicLabel>
    {
        public void Configure(EntityTypeBuilder<MusicLabel> builder)
        {
            builder.HasKey(e => e.IdMusicLabel);
            builder.Property(e => e.IdMusicLabel).ValueGeneratedOnAdd();
            builder.Property(e => e.Name).IsRequired().HasMaxLength(50);

            builder.HasMany<Album>(e => e.Albums);
        }
    }
}