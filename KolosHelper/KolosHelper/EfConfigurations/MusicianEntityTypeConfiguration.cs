using KolosHelper.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KolosHelper.EfConfigurations
{
    public class MusicianEntityTypeConfiguration : IEntityTypeConfiguration<Musician>
    {
        public void Configure(EntityTypeBuilder<Musician> builder)
        {
            builder.HasKey(e => e.IdMusician);
            builder.Property(e => e.IdMusician).ValueGeneratedOnAdd();
            builder.Property(e => e.FirstName).IsRequired().HasMaxLength(30);
            builder.Property(e => e.LastName).IsRequired().HasMaxLength(50);
            builder.Property(e => e.Nickname).IsRequired().HasMaxLength(20);

            builder.HasMany<Musician_Track>(e => e.MusicianTracks);
        }
    }
}