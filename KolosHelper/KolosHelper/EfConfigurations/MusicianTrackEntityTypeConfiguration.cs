using KolosHelper.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KolosHelper.EfConfigurations
{
    public class MusicianTrackEntityTypeConfiguration : IEntityTypeConfiguration<Musician_Track>
    {
        public void Configure(EntityTypeBuilder<Musician_Track> builder)
        {
            builder.HasKey(e => e.IdMusician);
            builder.HasKey(e => e.IdTrack);

            builder.HasOne(e => e.Musician).WithMany(m => m.MusicianTracks).HasForeignKey(e => e.IdMusician);
            builder.HasOne(e => e.Track).WithMany(m => m.MusicianTracks).HasForeignKey(e => e.IdMusician);
        }
    }
}