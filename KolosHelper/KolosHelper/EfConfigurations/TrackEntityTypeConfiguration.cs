using KolosHelper.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KolosHelper.EfConfigurations
{
    public class TrackEntityTypeConfiguration : IEntityTypeConfiguration<Track>
    {
        public void Configure(EntityTypeBuilder<Track> builder)
        {
            builder.HasKey(e => e.IdTrack);
            builder.Property(e => e.IdTrack).ValueGeneratedOnAdd();
            builder.Property(e => e.Trackname).IsRequired().HasMaxLength(20);
            builder.Property(e => e.Duration).IsRequired();
            
            builder.HasOne(e => e.Album).WithMany(m => m.Tracks).HasForeignKey(e => e.IdMusicAlbum);
            builder.HasMany<Musician_Track>(e => e.MusicianTracks);
        }
    }
}