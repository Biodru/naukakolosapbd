using System;
using System.Collections.Generic;

namespace KolosHelper.Models
{
    public class Album
    {
        public int IdAlbum { get; set; }
        public string AlbumName { get; set; }
        public DateTime PublishDate { get; set; }
        public int IdMusicLabel { get; set; }

        //MARK: Relacje - album ma wiele ścieżek, ale tylko jedno MusicLabel
        public ICollection<Track> Tracks { get; set; }
        public MusicLabel MusicLabel { get; set; }
    }
}