using System;
using System.Collections.Generic;
using KolosHelper.EfConfigurations;
using Microsoft.EntityFrameworkCore;

namespace KolosHelper.Models
{
    public class MainDbContext : DbContext
    {

        public MainDbContext()
        {
            
        }
        
        public MainDbContext(DbContextOptions opt) : base(opt)
        {
            
        }
        
        //MARK: To tzreba zrobić i to ważne, bo inaczej tabelki się nie zrobią chyba

        public DbSet<Musician> Musicians { get; set; }
        public DbSet<Track> Tracks { get; set; }
        public DbSet<MusicLabel> MusicLabels { get; set; }
        public DbSet<Musician_Track> MusicianTracks { get; set; }
        public DbSet<Album> Albums { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //MARK: Seedowanie danymi
            List<Musician> musicians = new List<Musician>
            {
                new Musician
                {
                    IdMusician = 1,
                    FirstName = "Jan",
                    LastName = "Kowalski",
                    Nickname = "Jacek"
                },
                new Musician
                {
                    IdMusician = 2,
                    FirstName = "Janimna",
                    LastName = "Kowalska",
                    Nickname = "Jacekaaaa"
                },
                new Musician
                {
                    IdMusician = 3,
                    FirstName = "Janko",
                    LastName = "Muzykant",
                    Nickname = "Ehh"
                },
            };
            List<Album> albums = new List<Album>
            {
                new Album
                {
                    IdAlbum = 1,
                    AlbumName = "asdda",
                    PublishDate = DateTime.Now,
                    IdMusicLabel = 1
                },
                new Album
                {
                    IdAlbum = 2,
                    AlbumName = "fgaadfgadfgh",
                    PublishDate = DateTime.Now,
                    IdMusicLabel = 2
                },
                new Album
                {
                    IdAlbum = 3,
                    AlbumName = "fghadfdfg",
                    PublishDate = DateTime.Now,
                    IdMusicLabel = 3
                },
            };
            List<Musician_Track> musicianTracks = new List<Musician_Track>
            {
                new Musician_Track
                {
                    IdMusician = 1,
                    IdTrack = 1,
                },
                new Musician_Track
                {
                    IdMusician = 2,
                    IdTrack = 2,
                },
                new Musician_Track
                {
                    IdMusician = 3,
                    IdTrack = 3,
                },
            };
            List<MusicLabel> musicLabels = new List<MusicLabel>
            {
                new MusicLabel
                {
                    IdMusicLabel = 1,
                    Name = "fghdsjk"
                },
                new MusicLabel
                {
                    IdMusicLabel = 2,
                    Name = "fdfgdfgs"
                },
                new MusicLabel
                {
                    IdMusicLabel = 3,
                    Name = "fgddfgdfgs"
                },
            };
            List<Track> tracks = new List<Track>
            {
                new Track
                {
                    IdTrack = 1,
                    Trackname = "dfsaafs",
                    Duration = 4.14,
                    IdMusicAlbum = 1,
                },
                new Track
                {
                    IdTrack = 2,
                    Trackname = "rthhsgdfghs",
                    Duration = 4.4,
                    IdMusicAlbum = 2,
                },
                new Track
                {
                    IdTrack = 3,
                    Trackname = "hgdffghsfghs",
                    Duration = 7.7,
                    IdMusicAlbum = 3,
                },
            };
            
            //MARK: Zastosowanie konfiguracji i seedów
            
            modelBuilder.ApplyConfiguration(new AlbumEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MusicianEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MusicianTrackEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MusicLabelEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TrackEntityTypeConfiguration());
            modelBuilder.Entity<Album>(e => e.HasData(albums));
            modelBuilder.Entity<Musician>(e => e.HasData(musicians));
            modelBuilder.Entity<Musician_Track>(e => e.HasData(musicianTracks));
            modelBuilder.Entity<MusicLabel>(e => e.HasData(musicLabels));
            modelBuilder.Entity<Track>(e => e.HasData(tracks));
        }
    }
}