using System.Collections.Generic;

namespace KolosHelper.Models
{
    public class MusicLabel
    {
        public int IdMusicLabel { get; set; }
        public string Name { get; set; }
        
        
        //MARK: Wydawnictwo może wydawać wiele albumów
        public ICollection<Album> Albums { get; set; }
    }
}