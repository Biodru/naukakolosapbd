using System.Collections.Generic;

namespace KolosHelper.Models
{
    public class Musician
    {
        public int IdMusician { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nickname { get; set; }

        //MARK: Relacja - Wiele musician_track, bo to asocjacja. powiązanie tak naprawdę z traacks, wiele do wielu
        public ICollection<Musician_Track> MusicianTracks { get; set; }
    }
}