namespace KolosHelper.Models
{
    public class Musician_Track
    {
        public int IdTrack { get; set; }
        public int IdMusician { get; set; }

        //MARK: Relacje - jeden muzyk, jedna ścieżka, to tabela asocjacyjna
        public Musician Musician { get; set; }
        public Track Track { get; set; }
    }
}

