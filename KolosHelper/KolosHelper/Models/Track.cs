using System.Collections.Generic;

namespace KolosHelper.Models
{
    public class Track
    {
        public int IdTrack { get; set; }
        public string Trackname { get; set; }
        public double Duration { get; set; }
        public int? IdMusicAlbum { get; set; }
        
        //MARK: Relacje - ścieżka ma wiele powiązań z musician_track(asocjacja), ale znajduje się tylko na max 1 albumie, dlatego nullable
        public ICollection<Musician_Track> MusicianTracks { get; set; }
        public Album? Album { get; set; }
    }
}