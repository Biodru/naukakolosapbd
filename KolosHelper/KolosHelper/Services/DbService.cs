using System.Threading.Tasks;
using KolosHelper.DTOs;
using KolosHelper.Models;
using Microsoft.EntityFrameworkCore;

namespace KolosHelper.Services
{
    public class DbService : IDbService
    {
        private readonly MainDbContext _context;
        
        public DbService(MainDbContext context)
        {
            _context = context;
        }
        //MARK: Wyszukanie muzyka po id
        public async Task<GetMusicianByIdResponseDto> GetmusicianById(int id)
        {
            var musician = await _context.Musicians.Include(e => e.MusicianTracks).ThenInclude(e => e.Track).SingleAsync(e => e.IdMusician == id); //Mamy wyświetlić też utwory, więc trzeba zrobić powiązanie

            if (musician== null)
            {
                return new GetMusicianByIdResponseDto
                {
                    Exists = false,
                    Message = "Nie istnieje muzyk o podanym id",
                    Musician = null,
                };
            }

            return new GetMusicianByIdResponseDto
            {
                Exists = true,
                Message = "Istnieje taki muzyk",
                Musician = musician,
            };
        }

        public async Task<AddMusicianAndTrackResponseDto> AddMusician(AddMusicianAndTrackRequestDto musician)
        {
            //Dodajemy muzyka i utwór. Jeśli utwór nie istnieje to tworzymy
            var musicianE = await _context.Musicians.SingleAsync(m => m.IdMusician == musician.IdMusician);
            if (musicianE != null)
            {
                return new AddMusicianAndTrackResponseDto
                {
                    Added = false,
                    Message = "Istnieje już taki muzyk",
                };
            }

            var track = await _context.Tracks.SingleAsync(t => t.IdTrack == musician.IdTrack);
            if (track == null)
            {
                var nTrack = new Track
                {
                    //Można też bez id to robić, bo autoincrement
                    IdTrack = musician.IdTrack,
                    Trackname = musician.TrackName,
                    Duration = musician.Duration
                };
                await _context.Tracks.AddAsync(nTrack);
                track = nTrack;
            }

            var nMusician = new Musician
            {
                IdMusician = musician.IdMusician,
                FirstName = musician.FirstName,
                LastName = musician.LastName,
                Nickname = musician.Nickname,
            };

            var nMusicianAndTrack = new Musician_Track
            {
                IdMusician = nMusician.IdMusician,
                IdTrack = track.IdTrack,
            };

            await _context.Musicians.AddAsync(nMusician);
            await _context.MusicianTracks.AddAsync(nMusicianAndTrack);
            return new AddMusicianAndTrackResponseDto
            {
                Added = true,
                Message = $"Dodano pomyślnie nowego grajka: {nMusician.FirstName} {nMusician.LastName} {nMusician.Nickname}, jest teraz w bazie!",
            };
        }
    }
}