using System.Threading.Tasks;
using KolosHelper.DTOs;

namespace KolosHelper.Services
{
    public interface IDbService
    {
        public Task<GetMusicianByIdResponseDto> GetmusicianById(int id);
        public Task<AddMusicianAndTrackResponseDto> AddMusician(AddMusicianAndTrackRequestDto musician);

    }
}